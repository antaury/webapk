﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models.ViewModels
{
    public class VMZamestnanecsIndex
    {

        public int Id { get; set; }

        public string Jmeno { get; set; }

        public string Prijmeni { get; set; }
        
        [Display(Name = "Název oddělení")]
        public string NazevOddeleni { get; set; }

    }
}