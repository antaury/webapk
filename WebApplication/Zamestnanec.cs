namespace WebApplication
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Zamestnanec")]
    public partial class Zamestnanec
    {
        public int Id { get; set; }

        public string Jmeno { get; set; }

        public string Prijmeni { get; set; }

        public int? idOddeleni { get; set; }

        public virtual Oddeleni Oddeleni { get; set; }
    }
}
